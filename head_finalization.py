#!/usr/bin/python
# -*- coding:utf-8 -*-

# usage: python head_finalization.py <input> > <output>

def Head_Finalization(node): # HeadFinalization includes Coodination expection rule
	for e in node.iter():
		if len(e) > 1:
			if (e.get("cat") not in ["COOD",  "PN"]) and (e.get("xcat") not in ["COOD", "PN"]):
				node_head = e.get("head")
				lchild = e[0]
				lcat = lchild.get("cat")
				lid = lchild.get("id")
				rchild = e[1]
				rcat = rchild.get("cat")
				if (lcat not in ["PN", "''", "COOD"]) and (rcat not in ["PN", "''", "COOD"]):
					if node_head == lid:
						e.remove(lchild)
						e.append(lchild)

def Right_Recursive(node): # trace right node
	if node.tag == "tok":
		return node
	return Right_Recursive(node.getchildren()[-1])

def Find_Mainverb(node):
	head_id = node.get("head")
	if node.tag == "tok":
		arg0 = node.get("arg1")
		Insert_Particle(sentence, arg0, 0) 

	try:
		if node[0].get("id") == head_id:
			node = node[0]
			return Find_Mainverb(node)
		elif node[-1].get("id") == head_id:
			node = node[-1]
			return Find_Mainverb(node)
		else:
			return Find_Mainverb(node[-1])
	except:
		pass

def Insert_Particle(node, arg, argtype): # add new attri to sub and obj
	for e in node.iter():
		if e.get("id") == arg:
			if argtype == 0:
				Right_Recursive(e).set("va0", 1)
			elif argtype == 1:
				Right_Recursive(e).set("va1", 1)
			elif argtype == 2:
				Right_Recursive(e).set("va2", 1)
				pass

def Find_Argument(node): # Insert pseudo-particle after verb argument
	Find_Mainverb(node) # find main verb
	for e in node.iter():
		if e.get("cat") == "V":
			arg1 = e.get("arg1")
			Insert_Particle(node, arg1, 1)
			try:
				arg2 = e.get("arg2")
				if e.get("base") != "be":
					Insert_Particle(node, arg2, 2)
			except:
				pass


def Word_Extraction(node, mark): #Extract word from xml tree
	sentence = ""
	for e in node.iter():
		if e.text:
			pos_tag = e.get("pos")
			if pos_tag == "NNS":
				word = e.get("base")
				sentence += word + " "
				if e.get("va0"): sentence += "va0 "
				elif e.get("va1"): sentence += "va1 "
				if e.get("va2"): sentence += "va2 "

			elif e.get("base") in  ["a", "an", "the"]:
				pass

			else:
				word = e.text
				sentence += word + " "
				if e.get("va0"): sentence += "va0 "
				elif e.get("va1"): sentence += "va1 "
				if e.get("va2"): sentence += "va2 "

	if mark in [".", "?"]:
		sentence += mark

	sentence = sentence.encode('utf_8')

	print (sentence)

if __name__ == '__main__':
	import sys
	import xml.etree.ElementTree as ET
	from xml.etree.ElementTree import tostring

	file = open(sys.argv[1], "r")
	tree = ET.parse(file)
	root = tree.getroot()
	sentences = root.getchildren()

	for sentence in sentences:
		eos = tostring(sentence)[-13:-12]
		Head_Finalization(sentence)
		Find_Argument(sentence)
		Word_Extraction(sentence, eos)
